package tests;

import static org.junit.Assert.*;

import java.util.Calendar;

import org.junit.Test;

import clock.ClockModel;

public class ClockTests {

	@Test
	public void testClockModel() {
		//fail("Not yet implemented");
		//compare what you get from get methods of calendar to get methods of clock
		
		Calendar now = Calendar.getInstance();
		int second = now.get(Calendar.SECOND);
		int minute = now.get(Calendar.MINUTE);
		int hour = now.get(Calendar.HOUR_OF_DAY);
		ClockModel clock = new ClockModel();
		assertEquals("Test seconds construtor: ", second, clock.getSeconds());
		assertEquals("Test minutes constructor: ", minute, clock.getMinutes());
		assertEquals("Test hours constructor: ", hour, clock.getHours());
		
	}

	@Test
	public void testClockModelIntIntInt() {
		//fail("Not yet implemented");
		ClockModel clock = new ClockModel(5, 18, 16);				
		
		assertEquals("Test hours construtor: ", 5, clock.getHours());
		assertEquals("Test minutes constructor: ", 18, clock.getMinutes());
		assertEquals("Test seconds constructor: ", 16, clock.getSeconds());
		
	}

	@Test //done in class
	public void testSetTime() {
		//fail("Not yet implemented");
		ClockModel clock = new ClockModel();
		int hour = 12;
		int minute = 55;
		int second = 58;		
		clock.setTime(hour, minute, second);		
		
				
		assertEquals("Test hour: ", hour, clock.getHours());
		assertEquals("Test minute: ", minute, clock.getMinutes());
		assertEquals("Test minute: ", second, clock.getSeconds());
	}

	@Test
	public void testIncrementSeconds() {
		//fail("Not yet implemented");
		ClockModel clock = new ClockModel();
		int hour = 10;
		int minute = 45;
		int second = 58;		
		clock.setTime(hour, minute, second);
		clock.incrementSeconds(); //increments normally
		
		assertEquals("Test increment seconds: ", second+1, clock.getSeconds());
		clock.incrementSeconds(); //exceptional case, second's reach 60 and roll over
		
		assertEquals("Test increment seconds-wrap: ", 0, clock.getSeconds());
		assertEquals("Test increment seconds-wrap to minute: ", minute+1, clock.getMinutes());
		
		ClockModel clock2 = new ClockModel();
		int hr = 10;
		int min = 59;
		int sec = 59;		
		clock2.setTime(hr, min, sec);		
		clock2.incrementSeconds(); //exceptional case, second's reach 60 and minutes reach 60 to roll over
		
		assertEquals("Test increment seconds-wrap: ", 0, clock2.getSeconds());
		assertEquals("Test increment seconds-wrap to minute: ", 0, clock2.getMinutes());
		assertEquals("Test increment minutes-wrap to hour: ", hr+1, clock2.getHours());
		
	}

	@Test //first half done in class
	public void testIncrementMinutes() {
		//fail("Not yet implemented");
		ClockModel clock = new ClockModel();
		int hour = 10;
		int minute = 45;
		int second = 50;		
		clock.setTime(hour, minute, second);
		clock.incrementMinutes();
		
		assertEquals("Test increment minute: ", minute+1, clock.getMinutes());
		
		ClockModel clock2 = new ClockModel();
		int hr = 10;
		int min = 59;
		int sec = 50;		
		clock2.setTime(hr, min, sec);
		clock2.incrementMinutes();
		
		assertEquals("Test increment minute-wrap to hour: ", hr+1, clock2.getHours());			
		assertEquals("Test increment minute-wrap: ", 0, clock2.getMinutes());
		
	}

//	@Test
//	public void testIncrementHours() {
//		fail("Not yet implemented");
//	}

}
